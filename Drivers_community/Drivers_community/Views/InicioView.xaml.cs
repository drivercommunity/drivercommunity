﻿using System;
using System.Collections.Generic;
using System.Net;
using Drivers_community.Languages;
using Xamarin.Forms;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;

namespace Drivers_community
{
    public partial class InicioView : ContentPage
    {
        InicioViewModel inicioViewModel;
        AppLinkEntry appLink;

        public InicioView()
        {
            InitializeComponent();

            inicioViewModel = App.Locator.InicioViewModel;
            BindingContext = inicioViewModel;

            NavigationPage.SetHasNavigationBar(this, false);

            var btLoginClick = new TapGestureRecognizer();
            btLoginClick.Tapped += (sender, e) =>
            {
                Login();
            };
            btLoginClick.SetBinding(TapGestureRecognizer.CommandProperty, "BTEntrar");
            gridLogin.GestureRecognizers.Add(btLoginClick);


            Title = "Enlaces";
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            tbPass.Text = GlobalElements.comprobarkeyPropertiesString("password");
            tbCorreo.Text = GlobalElements.comprobarkeyPropertiesString("usuario");

            var pageType = GetType().ToString();
            /*appLink = new AppLinkEntry
            {
                // It's not really clear why this must be a URI. At least on iOS it is just used as a unique identifier
                // and the URI is turned into a string again. Maybe for Android?
                AppLinkUri = new Uri(string.Format("http://{0}/", App.AppLinkUri), UriKind.RelativeOrAbsolute),
                //AppLinkUri = new Uri(string.Format(App.AppLinkUri, Title).Replace(" ", "_")),
                Thumbnail = ImageSource.FromFile("logo.png"),
                Description = string.Format($"This is item {Title}"),
                Title = string.Format($"Item {Title}"),
                // Mark this as the current activity. This will for instance allow handoff operations on iOS.
                IsLinkActive = true
            };
            Application.Current.AppLinks.RegisterLink(appLink);*/
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            /*if (appLink != null)
            {
                appLink.IsLinkActive = false;
            }
            Application.Current.AppLinks.RegisterLink(appLink);*/
        }
        internal async void Login()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            //hacemos las validaciones
            Validacion _validacion = new Validacion();
            if (tbCorreo.Text != null)
            {
                _validacion.Requerido(tbCorreo.Text, "Correo electronico");
                _validacion.Email(tbCorreo.Text, "Correo electronico");
            }
            else
            {
                _validacion.nuevoMensajeValidacion(MensajeValidacion.TipoMensajeValidacion.Error, "Correo electronico");
            }
            //Password
            _validacion.Requerido(tbPass.Text, "Contraseña");

            if (!_validacion.Valido)
            {
                //KO
                tbCorreo.PlaceholderColor = Color.FromHex("#575757");
                tbPass.PlaceholderColor = Color.FromHex("#575757");
                tbCorreo.TextColor = Color.FromHex("#575757");
                tbPass.TextColor = Color.FromHex("#575757");

                if (_validacion.Mensajes.Count > 0)
                {
                    string mensajeError = AppResources.MisDatosMensajeError;
                    foreach (MensajeValidacion mensaje in _validacion.Mensajes)
                    {
                        if (mensaje.Texto == "Correo electronico")
                        {
                            tbCorreo.PlaceholderColor = Color.Red;
                            tbCorreo.TextColor = Color.Red;
                            mensajeError += "\n" + "- " + AppResources.Inicio_Email;
                        }
                        if (mensaje.Texto == "Contraseña")
                        {
                            tbPass.PlaceholderColor = Color.Red;
                            tbPass.TextColor = Color.Red;
                            mensajeError += "\n" + "- " + AppResources.Inicio_Password;
                        }

                    }

                    if (mensajeError != "")
                        await DisplayAlert("", mensajeError, AppResources.Ok);
                    _validacion.Mensajes.Clear();
                }
            }
            else
            {
                GlobalElements.addkeyPropertiesString("usuario", tbCorreo.Text);
                GlobalElements.addkeyPropertiesString("password", tbPass.Text);

                inicioViewModel.goToView();
            }
            IsBusy = false;
        }
        void OnSaveActivated(object sender, EventArgs e)
        {
            //appLink = GetAppLink();
            //Application.Current.AppLinks.RegisterLink(appLink);
        }

        void OnDeleteActivated(object sender, EventArgs e)
        {
            //Application.Current.AppLinks.DeregisterLink(appLink);
        }
        void OnCancelActivated(object sender, EventArgs e)
        {

        }

    }
}
