﻿using System;
using System.Collections.Generic;
using System.Text;
using Drivers_community.Languages;
using Xamarin.Forms;

namespace Drivers_community
{
    public partial class Drivers_communityWebView : ContentPage
    {
        Drivers_communityWebViewModel Drivers_communityWebViewModel;
        WebView WvTerminos;
        public static string URL_actual_a_la_que_navegar;

        public Drivers_communityWebView()
        {
            InitializeComponent();

            Drivers_communityWebViewModel = App.Locator.Drivers_communityWebViewModel;
            BindingContext = Drivers_communityWebViewModel;

            NavigationPage.SetHasNavigationBar(this, false);

            WvTerminos = new WebView() { HorizontalOptions = LayoutOptions.FillAndExpand, VerticalOptions = LayoutOptions.FillAndExpand };
            WvTerminos.Navigating += WebOnNavigating;
            WvTerminos.Navigated += WebOnNavigatedAsync;


        }
        protected override void OnAppearing()
        {
            base.OnAppearing();

            contentCargando.IsVisible = true;

            if (GlobalElements.comprobarAccesoInternet())
            {
                string user = GlobalElements.comprobarkeyPropertiesString("usuario") + Entornos.URL_SEPARADOR + GlobalElements.comprobarkeyPropertiesString("password");
                byte[] bit = Encoding.UTF8.GetBytes(user);

                string userEncode = Convert.ToBase64String(bit);
                userEncode = System.Net.WebUtility.UrlEncode(userEncode);

                GlobalElements.printDebugTrace(userEncode);
                string direccion = GlobalElements.direccionURL != "" ?"&startURL=" + GlobalElements.direccionURL : "";
                URL_actual_a_la_que_navegar = Entornos.URL_LOGIN + "un=" + userEncode + direccion;
                WvTerminos.Source = new UrlWebViewSource { Url = URL_actual_a_la_que_navegar };
                ContenidoPagina.Content = WvTerminos;

            }else
            {
                Drivers_communityWebViewModel.goBack();

            }

            Xamarin.Forms.MessagingCenter.Unsubscribe<ViewModelBase>(this, "ChangeUrl");
            Xamarin.Forms.MessagingCenter.Subscribe<ViewModelBase>(this, "ChangeUrl", (sender) =>
            {
                OnAppearing();
            });
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        private void WebOnNavigatedAsync(object sender, WebNavigatedEventArgs e)
        {
            GlobalElements.printDebugTrace(e.Url);
            if (e.Url.Contains(Entornos.URL_ERROR))
            {
                //Ha ocurrido un error en el login
                GlobalElements.addkeyPropertiesString("password", "");
                GlobalElements.direccionURL = "";
                DisplayAlert(AppResources.ErrorTitulo, AppResources.ErrorDatosLogin, AppResources.Ok);
                Drivers_communityWebViewModel.goBack();


            }
            if (e.Url == "" && (e.Result == WebNavigationResult.Failure || e.Result == WebNavigationResult.Timeout))
            {
                GlobalElements.addkeyPropertiesString("password", "");
                GlobalElements.direccionURL = "";
                Drivers_communityWebViewModel.goBack();
                DisplayAlert(AppResources.ErrorTitulo, AppResources.falloOpenUrl, AppResources.Ok);
            }
            /*if(e.Url.Contains(Entornos.URL_LOGO) && e.Result == WebNavigationResult.Success){
                ContenidoPagina.IsVisible = true;
                contentCargando.IsVisible = false;
            }*/
            if (e.Result == WebNavigationResult.Success)
            {
                ContenidoPagina.IsVisible = true;
                contentCargando.IsVisible = false;
            }

        }

        private void WebOnNavigating(object sender, WebNavigatingEventArgs e)
        {
            //GlobalElements.printDebugTrace(e.Url);
            if(e.Url.Contains(Entornos.URL_LOGIN_OUT)){
                GlobalElements.addkeyPropertiesString("password", "");
                GlobalElements.addkeyPropertiesString("usuario", "");
                GlobalElements.direccionURL = "";
                Drivers_communityWebViewModel.goBack();
            }

        }

    }
}
