﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Threading.Tasks;
namespace Drivers_community
{
    public class NavigationService : INavigationService
    {
        public NavigationService()
        {
        }
        private IDictionary<Type, Type> _viewModelRouting = new Dictionary<Type, Type>()
        {
            { typeof(InicioViewModel),  typeof(InicioView) },
            { typeof(Drivers_communityWebViewModel),  typeof(Drivers_communityWebView) },
        };

        public void NavigateTo<TDestinationViewModel>(object[] navigationContext = null)
        {
            Type pageType = _viewModelRouting[typeof(TDestinationViewModel)];
            var page = Activator.CreateInstance(pageType, navigationContext) as Page;

            if (page != null)
                Application.Current.MainPage.Navigation.PushAsync(page);
        }

        public void NavigateTo(Type destinationType, object[] navigationContext = null)
        {
            Type pageType = _viewModelRouting[destinationType];
            var page = Activator.CreateInstance(pageType, navigationContext) as Page;

            if (page != null)
                Application.Current.MainPage.Navigation.PushAsync(page);
            
        }

        public void NavigateBack()
        {
            if (getNavigationStack().Count > 0)
                Application.Current.MainPage.Navigation.PopAsync();
        }

        public void PopToRootAsync()
        {
            Application.Current.MainPage.Navigation.PopToRootAsync();
        }

        public IReadOnlyList<Page> getNavigationStack()
        {
            return Application.Current.MainPage.Navigation.NavigationStack;
        }

        public void RemovePage(Page page)
        {
            Application.Current.MainPage.Navigation.RemovePage(page);
        }

        public void InsertPageBeforeThis<TDestinationViewModel>(object[] navigationContext = null)
        {
            Type pageType = _viewModelRouting[typeof(TDestinationViewModel)];
            var page = Activator.CreateInstance(pageType, navigationContext) as Page;

            var count = Application.Current.MainPage.Navigation.NavigationStack.Count;
            Page actualPage = Application.Current.MainPage.Navigation.NavigationStack[count - 1];

            if (page != null)
                Application.Current.MainPage.Navigation.PushAsync(page);
        }

        public void RemoveNavigationStack()
        {
            try
            {
                //if (Application.Current.MainPage.GetType() == typeof(SplashScreenView))
                 //   return;
                var countList = Application.Current.MainPage.Navigation.NavigationStack.Count;
                GlobalElements.printDebugTrace("Cantidad de paginas en el NavigationStack: " + countList);
                if (countList > 2) //todo llamar aqui siempre despues de haber invocado al navigateTo
                { //para no eliminar la home ni la actual
                    for (var i = 0; i < countList - 2; i++)
                    {
                        GlobalElements.printDebugTrace("i: " + i);
                        this.RemovePage(this.getNavigationStack()[1]);
                    }
                }
            }
            catch (Exception e)
            {
                GlobalElements.printDebugTrace(e.StackTrace);
            }
        }
        #region MODALES
        public IReadOnlyList<Page> getModalNavigationStack()
        {
            return Application.Current.MainPage.Navigation.ModalStack;
        }

        public void ModalNavigateTo<TDestinationViewModel>(object[] navigationContext = null)
        {
            Type pageType = _viewModelRouting[typeof(TDestinationViewModel)];
            var page = Activator.CreateInstance(pageType, navigationContext) as Page;

            if (page != null)
                Application.Current.MainPage.Navigation.PushAsync(page);
        }

        public void PushModalAsync(Page page)
        {
            if (page != null)
                Application.Current.MainPage.Navigation.PushModalAsync(page);
        }

        public Task<Page> PopModalAsync()
        {
            return Application.Current.MainPage.Navigation.PopModalAsync();
        }
        #endregion
    }
}
