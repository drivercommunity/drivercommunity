﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Threading.Tasks;

namespace Drivers_community
{
    public interface INavigationService
    {
        void NavigateTo<TDestinationViewModel>(object[] navigationContext = null);

        void NavigateTo(Type destinationType, object[] navigationContext = null);

        void NavigateBack();

        void PopToRootAsync();

        void InsertPageBeforeThis<TDestinationViewModel>(object[] navigationContext = null);

        void RemovePage(Page page);

        void RemoveNavigationStack();

        IReadOnlyList<Page> getNavigationStack();

        IReadOnlyList<Page> getModalNavigationStack();

        void ModalNavigateTo<TDestinationViewModel>(object[] navigationContext = null);

        void PushModalAsync(Page page);

        Task<Page> PopModalAsync();
    }
}
