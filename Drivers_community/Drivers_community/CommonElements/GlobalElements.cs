﻿using System;
using System.Threading.Tasks;
using Drivers_community.Languages;
using Plugin.Connectivity;
using Xamarin.Forms;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Plugin.Settings;
using Plugin.Settings.Abstractions;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using Drivers_community.CommonElements;
using Newtonsoft.Json;

namespace Drivers_community
{
    public class GlobalElements
    {
        #region CONSTANTES 

        public const Boolean DEBUG = true;

        #endregion

        #region VARIABLES 

        public static string direccionURL = "";
        public static string messageGps = "";
        public static Boolean activeApp = false;
        #endregion

        #region LOCALIZACION
        private static string _idiomaApp;
        public static string IdiomaApp { get { return _idiomaApp; } set { _idiomaApp = value; } }

        public static void InitializeLanguages()
        {
            // This lookup NOT required for Windows platforms - the Culture will be automatically set
            if (Device.OS == TargetPlatform.iOS || Device.OS == TargetPlatform.Android)
            {
                // determine the correct, supported .NET culture
                var ci = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
                AppResources.Culture = ci; // set the RESX for resource localization
                DependencyService.Get<ILocalize>().SetLocale(ci); // set the Thread for locale-aware methods

                //establecer idioma en GlobalElements para facilitarselo a los WebServices y a la URL del WebView
                IdiomaApp = ci.TwoLetterISOLanguageName;
                printDebugTrace("Separador decimales: " + ci.NumberFormat.CurrencyDecimalSeparator);
            }

            printDebugTrace(IdiomaApp);
        }
        #endregion

        #region METODOS VARIOS
        public static void printDebugTrace(String message)
        {
            if (DEBUG)
                System.Diagnostics.Debug.WriteLine(message);
        }

        public static bool comprobarAccesoInternet()
        {
            if (!CrossConnectivity.Current.IsConnected)
            {
                return false;
            }
            return true;
        }

        public static bool canSendLocation()
        {
            if (comprobarAccesoInternet() && comprobarkeyPropertiesString("usuario") != "" && comprobarkeyPropertiesString("password") != "")
            {
                return true;
            }
            return false;
        }


        internal static void OpenURL(string url)
        {
            try
            {
                Device.OpenUri(new Uri(url));
            }
            catch (Exception)
            {
                GlobalElements.toastLong(AppResources.falloOpenUrl);
            }
        }
        public static void toastLong(string mensaje)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                DependencyService.Get<IToastMessage>().longMessage(mensaje);
            });
        }
        public static void addkeyPropertiesString(string key, string valor)
        {
            if (!App.Current.Properties.ContainsKey(key))
            {
                App.Current.Properties.Add(key, valor); ;
                CrossSettings.Current.AddOrUpdateValue(key, valor);
            }
            else
            {
                App.Current.Properties[key] = valor;
                CrossSettings.Current.AddOrUpdateValue(key, valor);
            }
        }
        public static string comprobarkeyPropertiesString(string key)
        {
            string valorKey = "";

            if (App.Current.Properties.ContainsKey(key))
            {
                valorKey = App.Current.Properties[key].ToString();
                return valorKey;
            }
            return valorKey;
        }

        //get the data to the send the position in a post call
        public static async void Get_Gps_position(Position position)
        {
            APIResponse res = await CheckAuth();
            if (res.Auth)
            {
                string direccionURL = "";
                var locator = CrossGeolocator.Current;
                locator.DesiredAccuracy = 50;
                string user = null;
                var client = new HttpClient();
                direccionURL += Entornos.direccionURL;

                if (CrossSettings.Current.GetValueOrDefault("usuario", null) != null)
                {
                    user = CrossSettings.Current.GetValueOrDefault("usuario", null);
                }

                if (user != null)
                {
                    direccionURL += "?lat=" + position.Latitude + "&long=" + position.Longitude + "&user=" + user;

                    var content = new StringContent("", Encoding.UTF8, "application/json");

                    if (direccionURL != "")
                    {
                        HttpResponseMessage response = await client.PostAsync(direccionURL, content);

                        var result = await response.Content.ReadAsStringAsync();
                    }
                }
                direccionURL = "";
            }
        }



        public static async Task<bool> CheckPermissions(Permission permission)
        {
            var permissionStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(permission);

            if (permissionStatus == PermissionStatus.Denied)
            {
                return false;
            }

            if ( permissionStatus != PermissionStatus.Granted)
            {
                var newStatus = await CrossPermissions.Current.RequestPermissionsAsync(permission);
                if (newStatus.ContainsKey(permission) && newStatus[permission] != PermissionStatus.Granted)
                {
                    return false;
                }
            }

            return true;
        }

        public static bool registeredUser()
        {
            //CheckPermissions(Permission.Location);      

            if (GlobalElements.comprobarkeyPropertiesString("usuario") != "" && GlobalElements.comprobarkeyPropertiesString("password") != "")
                return true;
            else
                return false;
        }

        public static async Task<APIResponse> CheckAuth()
        {
            if (CrossSettings.Current.GetValueOrDefault("usuario", null) != null)
            {
                var client = new System.Net.Http.HttpClient();
                string user = CrossSettings.Current.GetValueOrDefault("usuario", null);
                string authUrl = Entornos.authUrl;

                client.BaseAddress = new Uri(authUrl);
                var response = await client.GetAsync("?&user=" + user);

                response.EnsureSuccessStatusCode();
                var responseJSON = await response.Content.ReadAsStringAsync();

                var msg = JsonConvert.DeserializeObject<APIResponse>(responseJSON);

                return msg;
            }

            return null;
        }

        #endregion

    }

}
