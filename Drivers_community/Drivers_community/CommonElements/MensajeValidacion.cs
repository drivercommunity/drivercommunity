﻿using System;
namespace Drivers_community
{
    public class MensajeValidacion
    {
        public enum TipoMensajeValidacion
        {
            OK = 0,
            Error = 1,
            Warning = 2
        }

        #region Propiedades

        public TipoMensajeValidacion Tipo { get; set; }
        public string Texto { get; set; }


        #endregion
    }
}
