﻿using System;
using Plugin.Settings;
using Plugin.Settings.Abstractions;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Drivers_community.CommonElements;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using Plugin.Permissions;

namespace Drivers_community
{
    public class Region
    {
        #region UBICACION
        private static double _userLatitude = 0;
        private static double _userLongitude = 0;
        public static bool permitirUbicacion = false;
        public static IGeolocator locator;
        public static async void activarServicioDeUbicacion()
        {
            locator = CrossGeolocator.Current;

            //if (!locator.IsGeolocationAvailable || !locator.IsGeolocationEnabled)
            if (!locator.IsGeolocationAvailable || !locator.IsGeolocationEnabled)
            {
                permitirUbicacion = false;
                return;
            }
            locator.DesiredAccuracy = 50;
            var position = await locator.GetPositionAsync(TimeSpan.FromSeconds(10));
            if (position == null)
                return;

            UserLatitude = position.Latitude;
            UserLongitude = position.Longitude;
            permitirUbicacion = true;
            Console.WriteLine("GPS: " + UserLatitude + ", " + UserLongitude);

            /*if (locator.IsListening)
            await locator.StopListeningAsync();


            await locator.StartListeningAsync(10000, 5);
            locator.AllowsBackgroundUpdates = true;

            locator.DesiredAccuracy = 100;*/
            /*locator.PositionError += (sender, args) =>
            {
            if (args.Error == GeolocationError.Unauthorized)
            {
            UserDialogs.Instance.Alert(new AlertConfig
            {
            Message = "GPS is required.",
            OnOk = () => { viewModel.AreButtonsEnabled = false; }
            });
            }
            };*/
            //locator.PositionChanged += UpdatePosition;
        }

        public static void UpdatePosition(object sender, PositionEventArgs e)
        {
            /*UserLatitude = e.Position.Latitude;
            UserLongitude = e.Position.Longitude;
            permitirUbicacion = true;
            printDebugTrace("GPS: " + UserLatitude + ", " + UserLongitude);*/
        }
        #endregion
    }
}
