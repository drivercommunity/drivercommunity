﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drivers_community.CommonElements
{
    public class APIResponse
    {
        public string username { get; set; }
        public Boolean Auth { get; set; }
    }
}
