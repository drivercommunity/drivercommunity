﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Drivers_community
{
    public class Validacion
    {

        #region Propiedades

        public List<MensajeValidacion> Mensajes = new List<MensajeValidacion>();
        public bool Valido = true;

        #endregion

        #region Métodos

        /// <summary>
        /// Añade un nuevo mensaje de validación a la lista de mensajes
        /// </summary>
        /// <param name="tipo">Tipo del mensaje</param>
        /// <param name="mensaje">Texto del mensaje</param>
        public void nuevoMensajeValidacion(MensajeValidacion.TipoMensajeValidacion tipo, string mensaje)
        {
            if (Mensajes.FindAll(x => x.Tipo == tipo && x.Texto == mensaje).Count == 0)
                Mensajes.Add(new MensajeValidacion { Tipo = tipo, Texto = mensaje });

            // Si se añade un Error a los mensajes, se marca con estado no válido. 
            if (tipo == MensajeValidacion.TipoMensajeValidacion.Error)
            {
                Valido = false;
            }
        }

        #endregion

        #region Funciones de validacion

        /// <summary>
        /// Valida si un campo no es vacío
        /// </summary>
        /// <param name="valor">Valor del campo</param>
        /// <param name="mensaje_error">Mensaje del error</param>
        /// <returns>Verdadero si no es vacío</returns>
        public bool Requerido(string valor, string mensaje_error)
        {
            bool _result = !string.IsNullOrEmpty(valor);

            if (!_result) nuevoMensajeValidacion(MensajeValidacion.TipoMensajeValidacion.Error, mensaje_error);

            return _result;
        }

        /// <summary>
        /// Valida si un campo de email tiene el formato correcto
        /// </summary>
        /// <param name="valor">Valor del campo</param>
        /// <param name="mensaje_error">Mensaje del error</param>
        /// <returns>Verdadero si tiene el formato correcto</returns>
        public bool Email(string valor, string mensaje_error)
        {
            bool _result = true;

            if (!string.IsNullOrEmpty(valor))
            {
                string _pattern = @"^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$";
                Regex _re = new Regex(_pattern);

                _result = _re.IsMatch(valor);

                if (!_result) nuevoMensajeValidacion(MensajeValidacion.TipoMensajeValidacion.Error, mensaje_error);
            }

            return _result;
        }
        #endregion
    }
}
