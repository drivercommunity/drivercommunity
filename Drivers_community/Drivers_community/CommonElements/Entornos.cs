﻿using System;
namespace Drivers_community
{
    public static class Entornos
    {
        //public const string URL_LOGIN = "https://neurored-developer-edition.eu10.force.com/drivers/NEU_Drivers_Community_Login?";
        public const string URL_LOGIN = "https://neurored-developer-edition.eu10.force.com/drivers/NEU_Drivers_Community_Login?";
        public const string URL_GPS = "https://neurored-developer-edition.eu10.force.com/drivers/NEU_Drivers_Community_Login?";
        public static string direccionURL = "https://neuebuscm-developer-edition.eu10.force.com/apex/NEU_get_locationGps";
        public static string authUrl = "https://neuebuscm-developer-edition.eu10.force.com/apex/NEU_GetActiveShipment";
        public const string URL_LOGIN_OUT = "logout";
        public const string URL_SEPARADOR = "¡AppXamarin!";
        public const string URL_ERROR = "error=true";
        public const string URL_LOGO = "CommunitiesLanding";
        public const int timeRefreshGPS = 120; //time in seconds
        public const int metersRefreshGPS = 200; //meters to refresh GPS
        

    }
}
