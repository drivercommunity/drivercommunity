﻿using System;
using Drivers_community.Languages;
using Plugin.Connectivity;

namespace Drivers_community
{
    public class InicioViewModel : ViewModelBase
    {
        private readonly INavigationService _navigationService;

        public InicioViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        internal void goToView()
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                _navigationService.NavigateTo<Drivers_communityWebViewModel>();
            }
            else
            {
                GlobalElements.toastLong(AppResources.NoHayConexion);
            }

        }
    }
}
