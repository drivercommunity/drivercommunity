﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
namespace Drivers_community
{
    public class ViewModelBase : INotifyPropertyChanged
    {
        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                _isFree = !value;
                RaisePropertyChanged();
            }
        }
        private bool _isFree;
        public bool IsFree
        {
            get { return _isFree; }
            set
            {
                _isFree = value;
                _isBusy = !value;
                RaisePropertyChanged();
            }
        }

        public virtual void OnAppearing(object navigationContext)
        {
        }

        public virtual void OnDisappearing()
        {
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void RaisePropertyChanged([CallerMemberName]string propertyName = "")
        {
            var handle = PropertyChanged;
            if (handle != null)
                handle(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
