﻿using System;
using Unity;

namespace Drivers_community
{
    public class ViewModelLocator
    {
        readonly IUnityContainer _container;

        public ViewModelLocator()
        {
            _container = new UnityContainer();

            // ViewModels
            _container.RegisterType<ViewModelBase>();
            _container.RegisterType<InicioViewModel>();
            _container.RegisterType<Drivers_communityWebViewModel>();

            // Services     
            _container.RegisterType<INavigationService, NavigationService>();
        }

        public ViewModelBase ViewModelBase
        {
            get { return _container.Resolve<ViewModelBase>(); }
        }

        public InicioViewModel InicioViewModel
        {
            get { return _container.Resolve<InicioViewModel>(); }
        }

        public Drivers_communityWebViewModel Drivers_communityWebViewModel
        {
            get { return _container.Resolve<Drivers_communityWebViewModel>(); }
        }
    }
}
