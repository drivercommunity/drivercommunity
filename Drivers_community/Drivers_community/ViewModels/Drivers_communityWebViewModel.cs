﻿using System;
namespace Drivers_community
{
    public class Drivers_communityWebViewModel : ViewModelBase
    {
        private readonly INavigationService _navigationService;

        public Drivers_communityWebViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        internal void goBack()
        {
            App.setInicioView();
        }


    }
}
