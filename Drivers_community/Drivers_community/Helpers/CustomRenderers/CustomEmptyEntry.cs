﻿using System;
using Xamarin.Forms;

namespace Drivers_community
{
    public class CustomEmptyEntry : Entry
    {
        public CustomEmptyEntry()
        {
        }
        public static readonly BindableProperty PlaceholderTextColorProperty =
            BindableProperty.Create("PlaceholderTextColor", typeof(Color), typeof(CustomEmptyEntry), Color.Default);

        public Color PlaceholderTextColor
        {
            get { return (Color)GetValue(PlaceholderTextColorProperty); }
            set { SetValue(PlaceholderTextColorProperty, value); }
        }
    }
}
