﻿using System;
namespace Drivers_community
{
    public interface IToastMessage
    {
        void shortMessage(string mensaje);
        void longMessage(string mensaje);
    }
}
