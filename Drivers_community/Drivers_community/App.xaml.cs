﻿using Xamarin.Forms;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using System;
using Xamarin.Forms.Xaml;

namespace Drivers_community
{
    public partial class App : Application
    {
        private static ViewModelLocator _locator;
        public static NavigationPage inicioPage { get; set; }

        public static string AppName = "NeuroRed Drivers Community";
        public static string AppLinkUri = "www.neurored.com";

        public static ViewModelLocator Locator
        {
            get { return _locator = _locator ?? new ViewModelLocator(); }
        }

        public App()
        {
            InitializeComponent();
            GlobalElements.InitializeLanguages();
            if(GlobalElements.comprobarAccesoInternet()){
                if(GlobalElements.comprobarkeyPropertiesString("usuario") != "" && GlobalElements.comprobarkeyPropertiesString("password") != ""){
                    inicioPage = new NavigationPage(new Drivers_communityWebView());
                    Current.MainPage = inicioPage;
                }
                else{
                    setInicioView();
                }
            }
            else{
                setInicioView();
            }
        }

        public static void setInicioView()
        {
            NavigationPage nvPage = new NavigationPage(new InicioView());
            Current.MainPage = nvPage;
        }

        protected override void OnStart()
        {
            AppCenter.LogLevel = LogLevel.Verbose;//call that API before starting the SDK
                                                  //then go to the Log Flow section of Analytics
                                                  // Handle when your app starts
            AppCenter.Start("ios=83023133-14ef-4319-a8bf-a4b0fb225161;" + "android=f010ab6b-82a6-48df-8cc4-b69ae2854077", typeof(Analytics), typeof(Crashes));
            Analytics.SetEnabledAsync(true);
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
