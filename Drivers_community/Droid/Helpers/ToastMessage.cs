﻿using System;
using Android.App;
using Android.Widget;
using Drivers_community.Droid;

[assembly: Xamarin.Forms.Dependency(typeof(ToastMessage))]
namespace Drivers_community.Droid
{
    public class ToastMessage : IToastMessage
    {
        public void longMessage(string mensaje)
        {
            try
            {
                Toast t = Toast.MakeText(Application.Context, mensaje, ToastLength.Long);
                t.Show();
            }
            catch (Exception e)
            {
                GlobalElements.printDebugTrace(e.StackTrace);
            }
        }

        public void shortMessage(string mensaje)
        {
            try
            {
                Toast t = Toast.MakeText(Application.Context, mensaje, ToastLength.Short);
                t.Show();
            }
            catch (Exception e)
            {
                GlobalElements.printDebugTrace(e.StackTrace);
            }
        }
    }
}
