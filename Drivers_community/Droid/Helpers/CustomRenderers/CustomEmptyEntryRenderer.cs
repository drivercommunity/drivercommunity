﻿using Xamarin.Forms;
using Drivers_community;
using Drivers_community.Droid;
using Xamarin.Forms.Platform.Android;
using Android.Graphics;
using System.ComponentModel;
using System;
using Android.Widget;
using Android.Runtime;

[assembly: ExportRenderer(typeof(CustomEmptyEntry), typeof(CustomEmptyEntryRenderer))]
namespace Drivers_community.Droid
{
    public class CustomEmptyEntryRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if (Control != null)
            {
                Control.Background.SetColorFilter(Xamarin.Forms.Color.Transparent.ToAndroid(), PorterDuff.Mode.SrcIn);
                //Control.SetHintTextColor(Xamarin.Forms.Color.FromHex("#ffffff").ToAndroid());
                //Control.Typeface = Android.Graphics.Typeface.CreateFromAsset(this.Context.Assets, "LyonTextApp-Regular.ttf");
                Control.SetPadding(20, 25, 0, 0);
                IntPtr IntPtrtextViewClass = JNIEnv.FindClass(typeof(TextView));
                IntPtr mCursorDrawableResProperty = JNIEnv.GetFieldID(IntPtrtextViewClass, "mCursorDrawableRes", "I");
                JNIEnv.SetField(Control.Handle, mCursorDrawableResProperty, 0); // replace 0 with a Resource.Drawable.my_cursor
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if (this.Control == null) return;

            var customEntryEmpty = this.Element as CustomEmptyEntry;
            if (e.PropertyName == CustomEmptyEntry.PlaceholderTextColorProperty.PropertyName)
                this.SetPlaceholderTextColor(customEntryEmpty);
        }
        private void SetPlaceholderTextColor(CustomEmptyEntry view)
        {
            if (string.IsNullOrEmpty(view.Placeholder))
                return;
            Control.SetHintTextColor(view.PlaceholderTextColor.ToAndroid());
        }
    }
}
