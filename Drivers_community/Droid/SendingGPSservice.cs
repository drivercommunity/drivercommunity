﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading;
using Android.Util;
using Drivers_community.Languages;
using Plugin.Connectivity;
using Xamarin.Forms;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Plugin.Settings;
using Plugin.Settings.Abstractions;
using System.Collections.ObjectModel;
using System.Net.Http;
using Drivers_community.CommonElements;
using Newtonsoft.Json;

namespace Drivers_community.Droid
{
    [Service(Name = "com.xamarin.DriverCommunity")]
    public class SendingGPSservice : Service
    {
        static readonly string TAG = typeof(SendingGPSservice).FullName;
        static readonly int DELAY_BETWEEN_LOG_MESSAGES = 5000; // milliseconds
        static readonly int NOTIFICATION_ID = 10000;
        static readonly string direccionURLServer = Entornos.direccionURL;
        static readonly int timeRefreshGPS = Entornos.timeRefreshGPS; //time in seconds
        static readonly string authUrl = Entornos.authUrl;

        UtcTimestamper timestamper;
        bool isStarted;
        Handler handler;
        Action runnable;

        public override void OnCreate()
        {
            base.OnCreate();
            timestamper = new UtcTimestamper();
            handler = new Handler();

            // This Action is only for demonstration purposes.
            runnable = new Action(() =>
            {
                if (timestamper != null)
                {
                    handler.PostDelayed(runnable, DELAY_BETWEEN_LOG_MESSAGES);
                }
            });
        }

        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
        {
            if (!isStarted)
            {

                Log.Info(TAG, "OnStartCommand: The service is starting.");
                DispatchNotificationThatServiceIsRunning();
                handler.PostDelayed(runnable, DELAY_BETWEEN_LOG_MESSAGES);
                isStarted = true;

                //Set to repeat with alarm
                var intent2 = new Intent(this, typeof(RepeatingAlarm));
                var source = PendingIntent.GetBroadcast(this, 0, intent2, 0);
                //CurrentLocation Location = new CurrentLocation();
                // Schedule the alarm!
                var am = (AlarmManager)GetSystemService(AlarmService);

                //After 15s, use the RepeatingAlarm to show a toast
                am.SetExactAndAllowWhileIdle(AlarmType.ElapsedRealtimeWakeup, SystemClock.ElapsedRealtime() + timeRefreshGPS * 1000, source);


                //Position positionNow = callAsyncPosition().Result;

                Get_Gps_position();
                
                // This tells Android not to restart the service if it is killed to reclaim resources.
            }
            return StartCommandResult.Sticky;

        }

        private PowerManager getSystemService(object pOWER_SERVICE)
        {
            throw new NotImplementedException();
        }

        public override IBinder OnBind(Intent intent)
        {
            // Return null because this is a pure started service. A hybrid service would return a binder that would
            // allow access to the GetFormattedStamp() method.
            return null;
        }


        public override void OnDestroy()
        {
            // Stop the handler.
            handler.RemoveCallbacks(runnable);

            // Remove the notification from the status bar.
            var notificationManager = (NotificationManager)GetSystemService(NotificationService);
            notificationManager.Cancel(NOTIFICATION_ID);

            timestamper = null;
            isStarted = false;
            base.OnDestroy();
        }

        /// <summary>
        /// This method will return a formatted timestamp to the client.
        /// </summary>
        /// <returns>A string that details what time the service started and how long it has been running.</returns>
        string GetFormattedTimestamp()
        {
            return timestamper?.GetFormattedTimestamp();
        }

        void DispatchNotificationThatServiceIsRunning()
        {
            Notification.Builder notificationBuilder = new Notification.Builder(this)
                .SetSmallIcon(Resource.Drawable.abc_ab_share_pack_mtrl_alpha)
                .SetContentTitle(Resources.GetString(Resource.String.app_name))
                .SetContentText(Resources.GetString(Resource.String.notification_text));

            var notificationManager = (NotificationManager)GetSystemService(NotificationService);
            notificationManager.Notify(NOTIFICATION_ID, notificationBuilder.Build());
        }

        public static async void Get_Gps_position()
        {
            APIResponse res = await CheckAuth();
            Log.Info("jamarin debug", "Check if is auth to send gps");

            if (res.Auth)
            {
                Log.Info("jamarin debug", "Is auth");

                string direccionURL = "";
                Position position = null;
                var locator = CrossGeolocator.Current;
                locator.DesiredAccuracy = 50;
                string user = null;
                position = await locator.GetPositionAsync(TimeSpan.FromSeconds(60), null, true);
                var client = new HttpClient();
                direccionURL += direccionURLServer;

                if (CrossSettings.Current.GetValueOrDefault("usuario", null) != null)
                {
                    user = CrossSettings.Current.GetValueOrDefault("usuario", null);
                }
                Log.Info("jamarin debug", "Url Server"+direccionURL);

                if (user != null)
                {
                    direccionURL += "?lat=" + position.Latitude + "&long=" + position.Longitude + "&user=" + user;
                    var content = new StringContent("", Encoding.UTF8, "application/json");
                    Log.Info("jamarin debug", "Url Server" + direccionURL);

                    //only calling URL not necesary sending information
                    if (direccionURL != "")
                    {
                        HttpResponseMessage response = await client.PostAsync(direccionURL, content);

                        var result = await response.Content.ReadAsStringAsync();
                    }
                }
                direccionURL = "";
            }
        }

        private static async Task<APIResponse> CheckAuth()
        {
            Log.Info("jamarin debug", "Checking Auth");

            if (CrossSettings.Current.GetValueOrDefault("usuario", null) != null)
            {           
                var client = new System.Net.Http.HttpClient();
                string user = CrossSettings.Current.GetValueOrDefault("usuario", null);
                client.BaseAddress = new Uri(authUrl);
                var response = await client.GetAsync("?&user=" + user);
                Log.Info("jamarin debug", "Auth Url "+authUrl);

                response.EnsureSuccessStatusCode();
                var responseJSON = await response.Content.ReadAsStringAsync();

                var msg = JsonConvert.DeserializeObject<APIResponse>(responseJSON);

                return msg;
            }

            return null;
        }

    }

}