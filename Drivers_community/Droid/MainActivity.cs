﻿using System;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Xamarin.Forms;
using Drivers_community.Droid;
using Plugin.Settings;
using Plugin.Settings.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;

namespace Drivers_community.Droid
{
    [Activity(Label = "Drivers_community", Icon = "@drawable/icon", Theme = "@style/MyTheme", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Portrait, NoHistory = false)]
    [IntentFilter(new[] { Intent.ActionView },
        Categories = new[]
        {
            Android.Content.Intent.CategoryDefault,
            Android.Content.Intent.CategoryBrowsable
        },
        DataScheme = "https",
        DataPathPrefix = "",
        DataHost = "developer-Drivers_communitycustomers.cs87.force.com")]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        Intent serviceToStart;

        protected override void OnCreate(Bundle bundle)
        {
            Plugin.CurrentActivity.CrossCurrentActivity.Current.Activity = this;
            serviceToStart = new Intent(this, typeof(SendingGPSservice));

            base.OnCreate(bundle);
            global::Xamarin.Forms.Forms.Init(this, bundle);
            Intent outsideIntent = Intent;
            LoadApplication(new App());
            GlobalElements.activeApp = true;
            callSendLocation();
        }

        protected override void OnResume()
		{
			base.OnResume();
            if (Intent.Data != null)
            {
                /*AlertDialog.Builder CodeAD = new AlertDialog.Builder(this);

                CodeAD.SetTitle("Superduperapp");
                CodeAD.SetMessage("Code: " + Intent.Data.SchemeSpecificPart.Replace("//", String.Empty));
                CodeAD.Show();*/
                GlobalElements.direccionURL = Intent.Data.EncodedPath.Replace("/", String.Empty);
            }
            GlobalElements.activeApp = true;
            callSendLocation();
        }


        protected override void OnDestroy()
        {
            base.OnDestroy();
            GlobalElements.activeApp = false;                      
        }

        private void callSendLocation()
        {
            
            if (GlobalElements.registeredUser())
            {
                StartService(serviceToStart);
                StopService(serviceToStart);
            }
            else {
                /*
                Plugin.Permissions.CrossPermissions.Current.
                var results = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Location);
                //Best practice to always check that the key exists
                if (results.ContainsKey(Permission.Location))
                    status = results[Permission.Location];
                    */
            }
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Android.Content.PM.Permission[] grantResults)
        {
            Plugin.Permissions.PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

    }
}
