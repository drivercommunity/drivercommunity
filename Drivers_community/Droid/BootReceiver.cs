﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace Drivers_community.Droid
{
    [BroadcastReceiver(Enabled = true)]
    [IntentFilter(new[] { Intent.ActionBootCompleted })]
    public class BootReceiver : BroadcastReceiver
    {
        #region implemented abstract members of BroadcastReceiver
        public override void OnReceive(Context context, Intent intent)
        {
            Log.Info("Drivers Community", "Boot Received");

            try
            {
                //launching app                 

                if (intent.Action.Equals(Intent.ActionBootCompleted))
                {
                    Intent i = new Intent(context, typeof(SendingGPSservice));
                    context.StartService(i);
                    context.StopService(i);
                }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}