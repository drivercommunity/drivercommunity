﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Plugin.Geolocator;
using System.Net.Http;
using Plugin.Geolocator.Abstractions;
using Android.Util;

namespace Drivers_community.Droid
{
    [BroadcastReceiver]
    public class RepeatingAlarm : BroadcastReceiver
    {
        public static string direccionURL = "";
        Android.OS.PowerManager.WakeLock _wakeLock;

        public override void OnReceive(Context context, Intent intent)
        {
                //Starting Service For Sending GPS Position
                Intent serviceToStart;
                bool acquired = false;

                var pm = (PowerManager)context.ApplicationContext.GetSystemService(Context.PowerService);
                _wakeLock = pm.NewWakeLock((WakeLockFlags.ScreenDim | WakeLockFlags.AcquireCausesWakeup), "WakeDeviceReceiver");
                Log.Info("jamarin debug", "Wakelock: Acquire on RepeatingAlarm");

                if (!pm.IsInteractive)
                {
                    _wakeLock.Acquire();
                    acquired = true;
                }
                serviceToStart = new Intent(context, typeof(SendingGPSservice));
                serviceToStart.AddFlags(ActivityFlags.NewTask);
                context.StartService(serviceToStart);

                //Every time the `RepeatingAlarm` is fired, set the next alarm
                var intentForRepeat = new Intent(context, typeof(RepeatingAlarm));
                var source = PendingIntent.GetBroadcast(context, 0, intent, 0);
                var am = (AlarmManager)Android.App.Application.Context.GetSystemService(Context.AlarmService);
                am.SetExactAndAllowWhileIdle(AlarmType.ElapsedRealtimeWakeup, SystemClock.ElapsedRealtime() + 15 * 1000, source);

                context.StopService(serviceToStart);
                Log.Info("jamarin debug", "wake lock release RepeatingAlarm");
                
                if(acquired)
                    _wakeLock.Release();

        }

    }
}