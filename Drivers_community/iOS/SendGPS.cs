﻿using System;
using Plugin.Settings;
using Plugin.Settings.Abstractions;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Drivers_community.CommonElements;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using Plugin.Permissions;
using CoreLocation;
using UIKit;
using UserNotifications;
using Foundation;

namespace Drivers_community.iOS
{
    public class SendGPS
    {
        /*
        //get the data to the send the position in a post call
        public static async void Get_Gps_position(Position position)
        {
            APIResponse res = await CheckAuth();
            if (res.Auth)
            {
                string direccionURL = "";
                var locator = CrossGeolocator.Current;
                locator.DesiredAccuracy = 50;
                string user = null;
                var client = new HttpClient();
                direccionURL += Entornos.direccionURL;

                if (CrossSettings.Current.GetValueOrDefault("usuario", null) != null)
                {
                    user = CrossSettings.Current.GetValueOrDefault("usuario", null);
                }

                if (user != null)
                {
                    direccionURL += "?lat=" + position.Latitude + "&long=" + position.Longitude + "&user=" + user;
                 
                    var content = new StringContent("", Encoding.UTF8, "application/json");

                    if (direccionURL != "")
                    {
                        HttpResponseMessage response = await client.PostAsync(direccionURL, content);

                        var result = await response.Content.ReadAsStringAsync();
                    }
                }
                direccionURL = "";
            }
        }

        //check if the user have active shipments in System
        private static async Task<APIResponse> CheckAuth()
        {

            if (CrossSettings.Current.GetValueOrDefault("usuario", null) != null)
            {
                var client = new System.Net.Http.HttpClient();
                string authUrl = Entornos.authUrl;
                string user = CrossSettings.Current.GetValueOrDefault("usuario", null);
                client.BaseAddress = new Uri(authUrl);
                var response = await client.GetAsync("?&user=" + user);

                response.EnsureSuccessStatusCode();
                var responseJSON = await response.Content.ReadAsStringAsync();

                var msg = JsonConvert.DeserializeObject<APIResponse>(responseJSON);

                return msg;
            }

            return null;
        }
        */
        //set the recursive location sen 
        public async Task StartListening()
        {
            if (CrossGeolocator.Current.IsListening)
                return;

            ///This logic will run on the background automatically on iOS, however for Android and UWP you must put logic in background services. Else if your app is killed the location updates will be killed.
            await CrossGeolocator.Current.StartListeningAsync(TimeSpan.FromSeconds(5), 10, true, new Plugin.Geolocator.Abstractions.ListenerSettings
            {
                ActivityType = Plugin.Geolocator.Abstractions.ActivityType.AutomotiveNavigation,
                AllowBackgroundUpdates = true,
                DeferLocationUpdates = true,
                DeferralDistanceMeters = Entornos.metersRefreshGPS,
                DeferralTime = TimeSpan.FromSeconds(Entornos.timeRefreshGPS),
                ListenForSignificantChanges = true,
                PauseLocationUpdatesAutomatically = false
            });

            CrossGeolocator.Current.PositionChanged += UpdatePosition;
        }

        public static void UpdatePosition(object sender, PositionEventArgs e) => GlobalElements.Get_Gps_position(e.Position);

        //check the permissions for notifications
        public static void NotificationPermission()
        {
            if (UIDevice.CurrentDevice.CheckSystemVersion(10, 0))
            {
                // Ask the user for permission to get notifications on iOS 10.0+
                UNUserNotificationCenter.Current.RequestAuthorization(
                    UNAuthorizationOptions.Alert | UNAuthorizationOptions.Badge | UNAuthorizationOptions.Sound,
                    (approved, error) => { });

                // Watch for notifications while app is active
                UNUserNotificationCenter.Current.Delegate = new UserNotificationCenterDelegate();
            }
            else if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
            {
                // Ask the user for permission to get notifications on iOS 8.0+
                var settings = UIUserNotificationSettings.GetSettingsForTypes(
                    UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound,
                    new NSSet());

                UIApplication.SharedApplication.RegisterUserNotificationSettings(settings);
            }
        }
    }
}
