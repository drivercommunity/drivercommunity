﻿using System;
using Drivers_community.iOS;
using Foundation;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(ToastMessage))]
namespace Drivers_community.iOS
{
    public class ToastMessage : IToastMessage
    {
        const double LONG_DELAY = 3.0;
        const double SHORT_DELAY = 1.5;

        NSTimer alertDelay;
        UIAlertController alert;

        public void longMessage(string mensaje)
        {
            ShowAlert(mensaje, LONG_DELAY);
        }

        public void shortMessage(string mensaje)
        {
            ShowAlert(mensaje, SHORT_DELAY);
        }

        void ShowAlert(string message, double seconds)
        {
            alertDelay = NSTimer.CreateScheduledTimer(seconds, (obj) =>
            {
                dismissMessage();
            });
            alert = UIAlertController.Create(null, message, UIAlertControllerStyle.Alert);
            UIApplication.SharedApplication.KeyWindow.RootViewController.PresentViewController(alert, true, null);
        }

        void dismissMessage()
        {
            if (alert != null)
            {
                alert.DismissViewController(true, null);
            }
            if (alertDelay != null)
            {
                alertDelay.Dispose();
            }
        }
    }
}
