﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Text;

using Foundation;
using ObjCRuntime;
using UIKit;
using Plugin.Settings;
using Plugin.Settings.Abstractions;

using Plugin.LocalNotifications;

namespace Drivers_community.iOS
{
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        // class-level declarations
        NSUrlSession session;
        nint taskId;

        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            CrossLocalNotifications.Current.Show("Don´t close the app manually", "Remember, we cant send your location for active shipment if you close the app with home button", 101, DateTime.Now.AddSeconds(15));

            SendGPS.NotificationPermission();

            global::Xamarin.Forms.Forms.Init();

            LoadApplication(new App());

            return base.FinishedLaunching(app, options);
        }
		public override bool OpenUrl(UIApplication app, NSUrl url, NSDictionary options)
		{
            NSUrl urlBase = url;
            if (urlBase != null)
            {
                GlobalElements.direccionURL = urlBase.Path.Replace("/", String.Empty);
                Xamarin.Forms.MessagingCenter.Send(App.Locator.ViewModelBase, "ChangeUrl");
            }
            return true;
		}

        public override void WillTerminate(UIApplication uiApplication)
        {
            CrossLocalNotifications.Current.Show("Closed App", "The App was closed by the System, we cant send your location for active shipment if you don´t open the app", 101, DateTime.Now.AddSeconds(15));
            base.WillTerminate(uiApplication);
        }

        /// <Docs>Reference to the UIApplication that invoked this delegate method.</Docs>
        /// <remarks>Application are allocated approximately 5 seconds to complete this method. Application developers should use this
        /// time to save user data and tasks, and remove sensitive information from the screen.</remarks>
        /// <altmember cref="M:MonoTouch.UIKit.UIApplicationDelegate.WillEnterForeground"></altmember>
        /// <summary>
        /// Dids the enter background.
        /// </summary>
        /// <param name="application">Application.</param>
        public override void DidEnterBackground(UIApplication application)
        {
            Console.WriteLine("DidEnterBackground called...");

            // Ask iOS for additional background time and prepare upload.
            taskId = application.BeginBackgroundTask(delegate {
                if (taskId != 0)
                {
                    application.EndBackgroundTask(taskId);
                    taskId = 0;
                }
            });

            new System.Action(async delegate {

                await PrepareSend();

                application.BeginInvokeOnMainThread(delegate {
                    if (taskId != 0)
                    {
                        application.EndBackgroundTask(taskId);
                        taskId = 0;
                    }
                });

            }).BeginInvoke(null, null);
        }

        /// <summary>
        /// Prepares the upload.
        /// </summary>
        /// <returns>The upload.</returns>
        public async Task PrepareSend()
        {
            try
            {
                Console.WriteLine("PrepareSend called...");

                if (session == null)
                    session = InitBackgroundSession();

                //launching senderGPS
                SendGPS sender = new SendGPS();
                await sender.StartListening();

                // Check if task already exits
                var tsk = await GetPendingTask();
                if (tsk != null)
                {
                    Console.WriteLine("TaskId {0} found, state: {1}", tsk.TaskIdentifier, tsk.State);

                    // If our task is suspended, resume it.
                    if (tsk.State == NSUrlSessionTaskState.Suspended)
                    {
                        Console.WriteLine("Resuming taskId {0}...", tsk.TaskIdentifier);
                        tsk.Resume();
                    }

                    return; // exit, we already have a task
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("PrepareSend Ex: {0}", ex.Message);
            }
        }

        /// <summary>
        /// Gets the pending task.
        /// </summary>
        /// <returns>The pending task.</returns>
        private async Task<NSUrlSessionUploadTask> GetPendingTask()
        {
            NSUrlSessionUploadTask uploadTask = null;

            if (session != null)
            {
                var tasks = await session.GetTasksAsync();

                var taskList = tasks.UploadTasks;
                if (taskList.Count() > 0)
                    uploadTask = taskList[0];
            }

            return uploadTask;
        }

        /// <summary>
        /// Processes the completed task.
        /// </summary>
        /// <param name="sessionTask">Session task.</param>
        public void ProcessCompletedTask(NSUrlSessionTask sessionTask)
        {
            try
            {
                Console.WriteLine(string.Format("Task ID: {0}, State: {1}, Response: {2}", sessionTask.TaskIdentifier, sessionTask.State, sessionTask.Response));

                // Make sure that we have a response to process
                if (sessionTask.Response == null || sessionTask.Response.ToString() == "")
                {
                    Console.WriteLine("ProcessCompletedTask no response...");
                }
                else
                {
                    // Get response
                    var resp = (NSHttpUrlResponse)sessionTask.Response;

                    // Check that our task completed and server returned StatusCode 201 = CREATED.
                    if (sessionTask.State == NSUrlSessionTaskState.Completed && resp.StatusCode == 201)
                    {
                        // Do something with the uploaded file...
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("ProcessCompletedTask Ex: {0}", ex.Message);
            }
        }

        /// <summary>
        /// Initializes the background session.
        /// </summary>
        /// <returns>The background session.</returns>
        public NSUrlSession InitBackgroundSession()
        {
            // See URL below for configuration options
            // https://developer.apple.com/library/ios/documentation/Foundation/Reference/NSURLSessionConfiguration_class/index.html

            // Use same identifier for background tasks so in case app terminiated, iOS can resume tasks when app relaunches.
            string identifier = "MyBackgroundTaskId";

            using (var config = NSUrlSessionConfiguration.CreateBackgroundSessionConfiguration(identifier))
            {
                config.HttpMaximumConnectionsPerHost = 4; //iOS Default is 4
                config.TimeoutIntervalForRequest = 600.0; //30min allowance; iOS default is 60 seconds.
                config.TimeoutIntervalForResource = 120.0; //2min; iOS Default is 7 days
                return NSUrlSession.FromConfiguration(config, new UploadDelegate(), new NSOperationQueue());
            }
        }
    }

    public class UploadDelegate : NSUrlSessionTaskDelegate
    {
        // Called by iOS when the task finished trasferring data. It's important to note that his is called even when there isn't an error.
        // See: https://developer.apple.com/library/ios/documentation/Foundation/Reference/NSURLSessionTaskDelegate_protocol/index.html#//apple_ref/occ/intfm/NSURLSessionTaskDelegate/URLSession:task:didCompleteWithError:
        public override void DidCompleteWithError(NSUrlSession session, NSUrlSessionTask task, NSError error)
        {
            Console.WriteLine(string.Format("DidCompleteWithError TaskId: {0}{1}", task.TaskIdentifier, (error == null ? "" : " Error: " + error.Description)));

            if (error == null)
            {
                var appDel = UIApplication.SharedApplication.Delegate as AppDelegate;
                appDel.ProcessCompletedTask(task);
            }
        }

        // Called by iOS when session has been invalidated.
        // See: https://developer.apple.com/library/ios/documentation/Foundation/Reference/NSURLSessionDelegate_protocol/index.html#//apple_ref/occ/intfm/NSURLSessionDelegate/URLSession:didBecomeInvalidWithError:
        public override void DidBecomeInvalid(NSUrlSession session, NSError error)
        {
            Console.WriteLine("DidBecomeInvalid" + (error == null ? "undefined" : error.Description));
        }

        // Called by iOS when all messages enqueued for a session have been delivered.
        // See: https://developer.apple.com/library/ios/documentation/Foundation/Reference/NSURLSessionDelegate_protocol/index.html#//apple_ref/occ/intfm/NSURLSessionDelegate/URLSessionDidFinishEventsForBackgroundURLSession:
        public override void DidFinishEventsForBackgroundSession(NSUrlSession session)
        {
            Console.WriteLine("DidFinishEventsForBackgroundSession");
        }

        // Called by iOS to periodically inform the progress of sending body content to the server.
        // See: https://developer.apple.com/library/ios/documentation/Foundation/Reference/NSURLSessionTaskDelegate_protocol/index.html#//apple_ref/occ/intfm/NSURLSessionTaskDelegate/URLSession:task:didSendBodyData:totalBytesSent:totalBytesExpectedToSend:
        public override void DidSendBodyData(NSUrlSession session, NSUrlSessionTask task, long bytesSent, long totalBytesSent, long totalBytesExpectedToSend)
        {
            // Uncomment line below to see file upload progress outputed to the console. You can track/manage this in your app to monitor the upload progress.
            //Console.WriteLine ("DidSendBodyData bSent: {0}, totalBSent: {1} totalExpectedToSend: {2}", bytesSent, totalBytesSent, totalBytesExpectedToSend);
        }
    }
}
